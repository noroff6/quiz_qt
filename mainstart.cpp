#include "mainstart.h"
#include "ui_mainstart.h"

MainStart::MainStart(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainStart)
{
    ui->setupUi(this);
    // Set window size to 70% of screen
    resize(QGuiApplication::primaryScreen()->availableGeometry().size() * 0.7);

    // Change window title
    setWindowTitle("Welcome to the quiz game! ");


}

MainStart::~MainStart()
{
    delete ui;
}

void MainStart::on_lineEdit_textEdited(const QString &arg1)
{
    ui->lineEdit->setMaxLength(5);
    // Add reg-ex validation for characters only
}

/*void MainStart::on_lineEdit_returnPressed()
{
    QString name = ui->lineEdit->text();
    QMessageBox::information(this, "Text" ,"Welcome " + name);
}*/


void MainStart::on_pushButton_clicked()
{

    QString name = ui->lineEdit->text();
    //ui->statusbar->showMessage("Welcome " + name);
    //ui->label->show();
    //QString cs1="hover: color(255, 255, 255)";
    //ui->pushButton->setStyleSheet(cs1);
    //QMessageBox::information(this, "Text" ,"Welcome " + name);
    if(!name.isEmpty())
    {
        ui->stackedWidget->setCurrentIndex(2);
        ui->label_2->setText(name);
    }
    else
    {
        QMessageBox::warning(this, "Text" ,"Please add a name. Field cannot be empty. ");
    }

}



void MainStart::on_pushButton_4_clicked()
{
    points++;
    QString str_points = QString::number(points);
    ui->label_6->setText(str_points);
    QMessageBox::warning(this, "Text" ,"Corrext! Added " + str_points + " points");
    ui->stackedWidget->setCurrentIndex(3);
}

