#ifndef MAINSTART_H
#define MAINSTART_H

#include <QMainWindow>
#include <QtWidgets>
//#include <QHBoxLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class MainStart; }
QT_END_NAMESPACE

class MainStart : public QMainWindow
{
    Q_OBJECT

public:
    MainStart(QWidget *parent = nullptr);
    ~MainStart();

private slots:
    void on_lineEdit_textEdited(const QString &arg1);
    //void on_lineEdit_returnPressed();

    void on_pushButton_clicked();
    void on_pushButton_4_clicked();

private:
    Ui::MainStart *ui;
    int points = 0;
};
#endif // MAINSTART_H
