#include "mainstart.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainStart w;
    w.show();
    return a.exec();
}
